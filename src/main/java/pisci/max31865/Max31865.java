package pisci.max31865;

import java.io.BufferedWriter;
import java.io.File;

/*
 * #%L
 * **********************************************************************
 * ORGANIZATION  :  Pi4J
 * PROJECT       :  Pi4J :: Java Examples
 * FILENAME      :  SpiExample.java
 *
 * This file is part of the Pi4J project. More information about
 * this project can be found here:  http://www.pi4j.com/
 * **********************************************************************
 * %%
 * Copyright (C) 2012 - 2017 Pi4J
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.Scanner;

import com.pi4j.Pi4J;
import com.pi4j.context.Context;
import com.pi4j.io.gpio.digital.DigitalInput;
import com.pi4j.io.gpio.digital.DigitalOutput;
import com.pi4j.io.gpio.digital.DigitalState;
import com.pi4j.util.Console;

/**
 * This example code demonstrates how to perform basic SPI communications using
 * the Raspberry Pi. CS0 and CS1 (ship-select) are supported for SPI0.
 *
 * @author Robert Savage
 */
public class Max31865 {

	static final byte MAX31856_CONFIG_REG = 0b0000_0000; // 0x00
	static final byte MAX31856_CONFIG_BIAS = (byte) 0b1000_0000; // 0x80
	static final byte MAX31856_CONFIG_MODEAUTO = 0b0100_0000; // 0x40
	static final byte MAX31856_CONFIG_MODEOFF = 0b0000_0000; // 0x00
	static final byte MAX31856_CONFIG_1SHOT = 0b0010_0000; // 0x20;
	static final byte MAX31856_CONFIG_3WIRE = 0b0001_0000; // 0x10;
	static final byte MAX31856_CONFIG_24WIRE = 0b0000_0000; // 0x00;
	static final byte MAX31856_CONFIG_FAULTSTAT = 0b0000_0010; // 0x02;
	static final byte MAX31856_CONFIG_FILT50HZ = 0b0000_0001; // 0x01;
	static final byte MAX31856_CONFIG_FILT60HZ = 0b0000_0000; // 0x00;

	static final byte MAX31856_RTDMSB_REG = 0b0000_0001; // 0x01;
	static final byte MAX31856_RTDLSB_REG = 0b0000_0010; // 0x02;
	static final byte MAX31856_HFAULTMSB_REG = 0b0000_0011; // 0x03;
	static final byte MAX31856_HFAULTLSB_REG = 0b0000_0100; // 0x04;
	static final byte MAX31856_LFAULTMSB_REG = 0b0000_0101; // 0x05;
	static final byte MAX31856_LFAULTLSB_REG = 0b0000_0110; // 0x06;
	static final byte MAX31856_FAULTSTAT_REG = 0b0000_0111; // 0x07;

	static final byte MAX31865_FAULT_HIGHTHRESH = (byte) 0b1000_0000; // 0x80;
	static final byte MAX31865_FAULT_LOWTHRESH = 0b0100_0000; // 0x40;
	static final byte MAX31865_FAULT_REFINLOW = 0b0010_0000; // 0x20;
	static final byte MAX31865_FAULT_REFINHIGH = 0b0001_0000; // 0x10;
	static final byte MAX31865_FAULT_RTDINLOW = 0b0000_1000; // 0x08;
	static final byte MAX31865_FAULT_OVUV = 0b0000_0100; // 0x04;

	static final double RTD_A = 3.9083e-3;
	static final double RTD_B = -5.775e-7;

	// SPI operations

	public static double RTDnominal = 100d;
	public static double RefResistor = 430d;

	private DigitalOutput cs = null;
	private static DigitalOutput mosi = null;
	private static DigitalInput miso = null;
	private static DigitalOutput sclk = null;

	private static Context pi4j;
	private SpiMode spiMode;
	private int channel;
	private int speed;

	public enum Max31865NumwiresEnum {
		TWOWIRE, THREEWIRE, FOURWIRE;
	}

	public enum SpiMode {
		HARDWARE, SOFTWARE;
	}

	// create Pi4J console wrapper/helper
	// (This is a utility class to abstract some of the boilerplate code)
	protected static final Console console = new Console();

	public static void main(String args[]) throws InterruptedException, IOException {
		pi4j = Pi4J.newAutoContext();
		System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "INFO");

		console.println("============================== Max31865 ====================================");
		Long sleepTime;
		Boolean loopFlag;
		String directory = "./";
		if (args.length == 3) {
			sleepTime = Long.parseLong(args[0]);
			loopFlag = Boolean.parseBoolean(args[1]);
			directory = args[2];
		} else {
			Scanner keyboard = new Scanner(System.in);
			System.out.println("enter sleep time in milliseconds");
			sleepTime = keyboard.nextLong();
			keyboard.nextLine();
			System.out.println("loop continually? (true or false)");
			loopFlag = keyboard.nextBoolean();
			keyboard.nextLine();

			File dir = null;
			do {
				dir = new File(directory);
				System.out.println("directory to store temperature data");
				directory = keyboard.nextLine();
				dir = new File(directory);
				if (!dir.exists()) {
					System.out.println(directory + " does not exist.  Create? y/n/q");
					char response = keyboard.nextLine().trim().toUpperCase().charAt(0);
					switch (response) {
					case 'Q':
						System.exit(0);
						break;
					case 'Y':
						if (!new File(directory).mkdirs()) {
							System.out.println("problem creating directory, exiting");
							System.exit(0);
						}
						break;
					case 'N':
						dir = null;
						break;
					}
				}
			} while (dir == null || dir.exists() == false);

			keyboard.close();
		}

		// print program title/header
		console.title("<-- The Pi4J Project -->", "SPI test program using MAX31865 RTD board");

		// allow for user to exit program using CTRL-C
		console.promptForExit();

		SpiMode spiMode = SpiMode.SOFTWARE;
		Max31865 max31865a = null;
		Max31865 max31865b = null;

		Path path = Paths.get(directory, Utils.fileNameFormatter.format(new Date()) + ".csv");
		Path pathCurrent = Paths.get(directory, "current.csv");
		try {
			Files.delete(pathCurrent);
		} catch (IOException e) {
			console.println("could not delete " + pathCurrent.toAbsolutePath());
		}

		System.out.println("creating " + path.toAbsolutePath());
		Files.createFile(path);

		Files.createLink(pathCurrent, path);

		try (BufferedWriter writer = Files.newBufferedWriter(path)) {

			switch (spiMode) {
			case HARDWARE:
				int channel = 0;
				max31865a = new Max31865(Max31865NumwiresEnum.THREEWIRE, 8, 11, channel, 115200);
				break;
			case SOFTWARE:
				Max31865.provisionPins(10, 9, 11); // mosi,
				// miso,
				// sclk
				max31865a = new Max31865(Max31865NumwiresEnum.THREEWIRE, 8, "csA");
				max31865b = new Max31865(Max31865NumwiresEnum.THREEWIRE, 7, "csB");

				break;
			}

			// continue running program until user exits using CTRL-C
			while (console.isRunning()) {
				console.println("====================Probe A =====================");
				double temp = max31865a.temperature();
				console.println("temperature A = " + temp);
				console.println("====================Probe B =====================");
				temp = max31865b.temperature();
				console.println("temperature B = " + temp);

				Date now = new Date();
				String dataString = Utils.dateTimeFormatter.format(now) + ", " + now.getTime() + ", "
						+ max31865a.temperature() + ", " + max31865b.temperature() + System.lineSeparator();
				writer.write(dataString);
				writer.flush();

				if (!loopFlag) {
					break;
				}
				Thread.sleep(sleepTime);
			}
			console.emptyLine();

			writer.close();
		}
	}

	public static void provisionPins(int mosiPinNum, int misoPinNum, int sclkPinNum) {
		var config = DigitalOutput.newConfigBuilder(pi4j).provider("gpiod-digital-output").shutdown(DigitalState.LOW)
				.initial(DigitalState.LOW);

		mosi = pi4j.create(config.address(mosiPinNum).id("mosi"));
		sclk = pi4j.create(config.address(sclkPinNum).id("sclk"));

		var configIn = DigitalInput.newConfigBuilder(pi4j).provider("gpiod-digital-input");
		miso = pi4j.create(configIn.address(misoPinNum).id("miso"));

	}

	// Software (bitbang) SPI
	public Max31865(Max31865NumwiresEnum numWires, int csPinNumber, String csPinId) {
		console.println("Software SPI mode");
		spiMode = SpiMode.SOFTWARE;

		var config = DigitalOutput.newConfigBuilder(pi4j).provider("gpiod-digital-output").shutdown(DigitalState.LOW)
				.initial(DigitalState.HIGH);

		cs = pi4j.create(config.address(csPinNumber).id(csPinId));

		init(numWires);
	}

	// Hardware SPI
	public Max31865(Max31865NumwiresEnum numWires, int csPinNum, int sclkPinNum, int channel, int speed) {
		console.println("Hardware SPI mode");
		spiMode = SpiMode.HARDWARE;
		this.channel = channel;
		this.speed = speed;

		var config = DigitalOutput.newConfigBuilder(pi4j).provider("gpiod-digital-output").shutdown(DigitalState.LOW)
				.initial(DigitalState.LOW);

		cs = pi4j.create(config.address(csPinNum).id("cs"));
		sclk = pi4j.create(config.address(sclkPinNum).id("sclkPinNum"));

		// TODO?? hardware SPI
		// int fd = Spi.wiringPiSPISetup(channel, speed);
		// if (fd == -1) {
		// console.println(" ==>> SPI SETUP FAILED");
		// return;
		// }
		init(numWires);
	}

	public Max31865(Boolean dummy) {

	}

	/***************************************************
	 * This is a library for the Adafruit PT100/P1000 RTD Sensor w/MAX31865
	 * 
	 * Designed specifically to work with the Adafruit RTD Sensor ---->
	 * https://www.adafruit.com/products/3328
	 * 
	 * This sensor uses SPI to communicate, 4 pins are required to interface
	 * Adafruit invests time and resources providing this open source code, please
	 * support Adafruit and open-source hardware by purchasing products from
	 * Adafruit!
	 * 
	 * Written by Limor Fried/Ladyada for Adafruit Industries. BSD license, all text
	 * above must be included in any redistribution
	 ****************************************************/

	public void init(Max31865NumwiresEnum wires) {
		setWires(wires);
		enableBias(false);
		autoConvert(false);
		clearFault();
	}

	public byte readFault() {
		return read8(MAX31856_FAULTSTAT_REG);
	}

	public void clearFault() {
		byte t = read8(MAX31856_CONFIG_REG);
		t &= ~0x2C;
		t |= MAX31856_CONFIG_FAULTSTAT;
		write8(MAX31856_CONFIG_REG, t);
	}

	public void enableBias(boolean b) {
		byte t = read8(MAX31856_CONFIG_REG);
		if (b) {
			t |= MAX31856_CONFIG_BIAS; // enable bias
		} else {
			t &= ~MAX31856_CONFIG_BIAS; // disable bias
		}
		write8(MAX31856_CONFIG_REG, t);
	}

	public void autoConvert(boolean b) {
		byte t = read8(MAX31856_CONFIG_REG);
		if (b) {
			t |= MAX31856_CONFIG_MODEAUTO; // enable autoconvert
		} else {
			t &= ~MAX31856_CONFIG_MODEAUTO; // disable autoconvert
		}
		write8(MAX31856_CONFIG_REG, t);
	}

	public void setWires(Max31865NumwiresEnum wires) {
		byte t = read8(MAX31856_CONFIG_REG);
		if (wires == Max31865NumwiresEnum.THREEWIRE) {
			t |= MAX31856_CONFIG_3WIRE;
		} else {
			// 2 or 4 wire
			t &= ~MAX31856_CONFIG_3WIRE;
		}

		write8(MAX31856_CONFIG_REG, t);
	}

	public int readRTD() {
		clearFault();
		enableBias(true);
		try {
			Thread.sleep(10);
		} catch (InterruptedException e) {
			console.println(e.getMessage());
		}
		byte t = read8(MAX31856_CONFIG_REG);
		t |= MAX31856_CONFIG_1SHOT;

		write8(MAX31856_CONFIG_REG, t);
		try {
			Thread.sleep(65);
		} catch (InterruptedException e) {
			console.println(e.getMessage());
		}

		byte[] rtd = read16(MAX31856_RTDMSB_REG);
		int rtdShort = Utils.bytesToShort(rtd);
		// remove fault
		rtdShort >>= 1;

		return rtdShort;
	}

	/**********************************************/

	private byte[] read(byte register, int numBytes) {

		byte packet[] = new byte[numBytes];
		sclk.low();
		cs.low();
		register &= 0x7f; // make sure top bit is not set
		spixfer(register);

		for (int i = 0; i < numBytes; ++i) {
			packet[i] = spixfer((byte) 0xFF);
		}

		cs.high();

		return packet;
	}

	private byte spixfer(byte x) {
		byte reply = 0;

		switch (spiMode) {
		case HARDWARE:
			byte[] data = new byte[1];
			data[0] = x;
			// TODO hardware spi
			// reply = (byte) Spi.wiringPiSPIDataRW(channel, data, 1);
			reply = (byte) (reply & 0xFF);
			break;
		case SOFTWARE:
			for (int i = 7; i >= 0; i--) {
				reply <<= 1;
				sclk.high();

				byte bitmask = (byte) (1 << i);
				int bit = (Byte.toUnsignedInt(x) & bitmask);

				if (bit == 0) {
					mosi.low();
				} else {
					mosi.high();
				}

				sclk.low();
				if (miso.state().compareTo(DigitalState.HIGH) == 0) {
					reply |= 1;
				}
			}
		}

		return reply;
	}

	private byte read8(byte register) {
		byte[] data = read(register, 1);
		return data[0];
	}

	private byte[] read16(byte register) {
		byte[] data = read(register, 2);
		byte[] retData = new byte[2];
		retData[0] = data[1];
		retData[1] = data[0];
		return retData;
	}

	private void write8(byte register, byte data) {
		sclk.low();
		cs.low();
		spixfer((byte) (register | 0x80));
		spixfer(data);
		cs.high();

	}

	public double temperature() {
		// http://www.analog.com/media/en/technical-documentation/application-notes/AN709_0.pdf

		double Z1, Z2, Z3, Z4, Rt, temp;
		int RtInt;
		RtInt = readRTD();
		console.println("RtInt = " + RtInt);
		Rt = (double) RtInt / 32768d;
		console.println("Rt = " + Rt);
		Rt *= RefResistor;
		console.println("Rt = " + Rt);

		// Serial.print("Resistance: "); Serial.println(Rt, 8);
		console.println("Resistance is " + Rt);
		Z1 = -RTD_A;
		Z2 = RTD_A * RTD_A - (4 * RTD_B);
		Z3 = (4 * RTD_B) / RTDnominal;
		Z4 = 2 * RTD_B;

		temp = Z2 + (Z3 * Rt);
		temp = (Math.sqrt(temp) + Z1) / Z4;

		if (temp >= 0) {
			return temp;
		}

		// ugh.
		double rpoly = Rt;

		temp = -242.02d;
		temp += 2.2228 * rpoly;
		rpoly *= Rt; // square
		temp += 2.5859e-3 * rpoly;
		rpoly *= Rt; // ^3
		temp -= 4.8260e-6 * rpoly;
		rpoly *= Rt; // ^4
		temp -= 2.8183e-8 * rpoly;
		rpoly *= Rt; // ^5
		temp += 1.5243e-10 * rpoly;

		return temp;
	}
}
