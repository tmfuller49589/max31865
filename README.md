# max31865



## Compiling

mvn package

## Running

java -jar max31865-0.0.1-SNAPSHOT.jar

## History

2024-11-15: updated for Pi4J library, using gpiod since pigpio does not work with RPi5
2024-02-07: updated for new Pi4J library, wiringPi has been replaced with pigpio

## Setting static IP on RPi

Got this from ChatGPT


1. Set Up a Static IP on the Raspberry Pi 5
A. Open NetworkManager Configuration:
Since you are using NetworkManager, you will configure the Ethernet connection directly through its interface.

Open the terminal on your Raspberry Pi 5.

Create a new Ethernet connection: If you don't already have an Ethernet connection, you can create one with NetworkManager:

bash

sudo nmcli con add type ethernet ifname eth0 con-name eth-static

Assign a static IP address: Assign an IP address, subnet mask, and gateway manually. For example:

bash

sudo nmcli con mod eth-static ipv4.addresses 192.168.2.2/24 ipv4.method manual

sudo nmcli con up eth-static

192.168.2.2 is the static IP for the Raspberry Pi.

/24 indicates the subnet mask (255.255.255.0).

The method is set to manual because you don’t want DHCP.
Restart the connection: To apply the changes, restart the NetworkManager service or bring the connection up:

bash

sudo nmcli con up eth-static

2. Set Up a Static IP on Your Arch Linux Laptop

On the Arch Linux laptop, you'll do something similar using either NetworkManager or manually via the command line.

A. Use NetworkManager on Arch Linux (GUI):

Open the Network settings (through the GUI or terminal).

Go to Wired connection settings.

Set a static IP, such as 192.168.2.1/24, for your Ethernet connection.

Save and apply the settings.

B. Use nmcli to Configure Static IP:

If you prefer the command line:

Identify your Ethernet interface (likely named something like eth0 or enp3s0):

bash

ip link
Use nmcli to set the static IP:

bash

sudo nmcli con mod "Wired connection 1" ipv4.addresses 192.168.2.1/24 ipv4.method manual

sudo nmcli con up "Wired connection 1"

3. Test the Connection

Now that both your Raspberry Pi 5 and Arch Linux laptop have static IP addresses, they should be able to communicate.

Ping the Raspberry Pi from the laptop:

bash

ping 192.168.2.2

Ping the laptop from the Raspberry Pi: On the Raspberry Pi:

bash

ping 192.168.2.1

If both pings work, you have successfully established a direct Ethernet connection.

4. (Optional) Enable SSH for Remote Access

To remotely connect between the devices:

Enable SSH on the Raspberry Pi:

bash

sudo systemctl enable ssh

sudo systemctl start ssh

Connect via SSH from the Arch Linux laptop:

bash

ssh pi@192.168.2.2

You can now use the Ethernet cable for direct communication between your Raspberry Pi 5 and Arch Linux laptop.
